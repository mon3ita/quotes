# Quotes

Get all quotes from `Goodreads`.

## How to test it?

1. Download the project, by typing `git clone https://github.com/monzita/quotes` on your cli.

2. You have two options:
	1. To make your own `virtualenv` in the project folder and install its dependencies in that environment.
	2. To try run it, by installing all dependencies globally.

*If you already have `scrapy` (version 1.8.0), skip the next step.*

3. To install the dependencies of the project, type `pip install -r requirements.txt`.

That's it, now type `scrapy crawl [spider]`, where `spider` currently can be:

*	`goodreads`, which will give you all quotes from `goodreads`.

## LICENSE

MIT
