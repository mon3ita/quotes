# -*- coding: utf-8 -*-
import scrapy

from ..items import QuotesItem


class GoodreadsSpider(scrapy.Spider):
    name = "goodreads"
    allowed_domains = ["www.goodreads.com"]
    start_urls = [
        "https://www.goodreads.com/quotes",
        "https://www.goodreads.com/quotes/recently_added",
        "https://www.goodreads.com/quotes/recently_created",
    ]

    def parse(self, response):

        quotes = response.css(".quotes > .quote")
        item = QuotesItem()
        for quote in quotes:
            item["author"] = quote.css(".authorOrTitle::text").extract_first().strip()
            item["quote"] = quote.css(".quoteText::text").extract_first().strip()

            item["image_urls"] = quote.css(
                "a.leftAlignedImage > img::attr(src)"
            ).extract()

            likes = quote.css("a.smallText::text").extract_first().strip()
            item["likes"] = "".join([ch for ch in likes if ch.isdigit()])
            item["tags"] = ", ".join(
                [
                    tag.css("::text").extract_first().strip()
                    for tag in quote.css(".greyText.smallText.left > a")
                ]
            )
            yield item

        next_page = response.css("a.next_page::attr(href)").extract_first()
        if next_page:
            url = "https://{}{}".format(self.allowed_domains[0], next_page)
            yield scrapy.Request(url, callback=self.parse)
